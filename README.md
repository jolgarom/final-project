# Description
---
Laboratory of Molecular Biology and Genetics of Federal Almazov Medical Research Center regularly performs exom sequencing in patients. As a rule, prior to whole exom sequencing (WES), the analysis of the hereditary nature of the disease is carried out and these patients are sequenced on small panels with targeted enrichment of the genes described in the literature and known as causal in different type of cardiomyopathy. If the small panel does not produce results, then WES can be conduct and the analysis of rare variants using frequency databases allows identifying pathogenic variants in ~ 70% of such patients.
---
# Goal
---
Detecting unknown variants of CNV in patients with different types of idiopathic cardiomyopathies
---
# Methods
---
* <https://cnvkit.readthedocs.io/en/stable/> - cnvkit
* <https://github.com/imgag/ClinCNV> - ClinCNV
---
# Files description
---
/software/ClinCNV/
* setup files for ClinCNV
---
/clincnv/Snakefile
---
* ClinCNV's executable file for Snakemake. To run, simply type snakemake in terminal in project directory
---
/clincnv/Comfig.yaml
---
* ClinCNV's configuration file for Snakemake
---
/clincnv/prepare_reference.sh
* .sh file to prepare a BED file of reference for use with ClinCNV
---
/clincnv/rules/ - directory with pipeline's rules for clincnv
* 10_BedCoverage.smk - Calculate coverage in the target and off-target regions from BAM read depths
* 11_MergeFiles.smk - Merge files from cohort for target  and off-target regions
* 12_StartClinCNV.smk - Launch of ClinCNV
---
/cnvkit/Snakefile
---
* CNVkit's executable file for Snakemake. To run, simply type snakemake in terminal in project directory
---
/cnvkit/Comfig.yaml
---
* CNVkit's configuration file for Snakemake
---
/cnvkit/1_make_target_bed.sh
* .sh file to prepare a BED file of target regions for use with CNVkit
---
/cnvkit/2_make_antitarget_bed.sh
* .sh file to prepare a BED file of off-target regions for use with CNVkit
---
/cnvkit/rules/ - directory with pipeline's rules for cnvkit
* 3_target_coverage.smk - Calculate coverage in the target regions from BAM read depths
* 4_antitarget_coverage.smk - Calculate coverage in the off-target regions from BAM read depths
* 5_make_a_reference.smk - Create a “flat” reference of neutral copy number
* 6_from_cnn_to_cnr.smk - Combine the uncorrected target and off-target coverage tables (.cnn) and correct for biases in regional coverage and GC content, according to the given reference
* 7_from_cnr_to_cns.smk - Infer discrete copy number segments from the given coverage table
* 8_make_a_scatter.smk - Generate a static image scatter plot
* 9_make_a_heatmap.smk - Generate heatmap of the larger-scale CNVs in a cohort
---
# System requirements
---
* python 3
* anaconda3
---
# Links
---
* <https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-14-S11-S1>






